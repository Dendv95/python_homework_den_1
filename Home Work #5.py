from random import randint
#Home Work 5\ 1 part
def my_function(arg1, arg2):
    """
    Данная функция, принимающая два аргумента должна:
    - если оба аргумента относятся к числовым типам - вернуть их произведение,
    - если к строкам - соединить в одну строку и вернуть,
    - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
    в любом другом случае вернуть кортеж (tuple) из аргументов
    """
    if (type(arg1) == int or type(arg1) == float) and (type(arg2) == int or type(arg2) == float):
        res_int = arg1 * arg2
        return res_int
    if type(arg1) == str and type(arg2) == str:
        res_str = arg1 + arg2
        return res_str
    if type(arg1) == str and type(arg2) != str:
        res_dict = {arg1: arg2}
        return res_dict
    if str(arg1) and str(arg2) == False:
        res_dict = {arg1: arg2}
        return res_dict
    else:
        res_tuple = (arg1, arg2)
        return res_tuple

res_int = my_function(2.2, 4)
print(res_int)
res_str = my_function('Hello', ' World')
print(res_str)
res_dict = my_function('Hello', 5)
print(res_dict)
print(type(res_dict))
res_tuple = my_function(3.2, [1])
print(res_tuple)
print(type(res_tuple))

#Home Work 5\ 2 part
def my_game():
    while True:
        random_number = randint(1, 100)
        random_phrase = randint(1, 3)
        print(random_number)
        user_input = input('Hi! Want to play the game? Choose your number from 1 to 100: ')
        try:
            user_input = int(user_input)
            if user_input > 100 or user_input <= 0:
                print(f'Your number is out of range!')
            else:
                break
        except:
            print(f'{user_input} is not a number. Please, choose number from 1 to 100.')
    if random_number == user_input:
        while True:
            user_input_2 = input('Holy shit, you won! Want to try again? [Y/N]: ')
            if user_input_2 == 'YES' or user_input_2 == 'Y' or user_input_2 == 'y' or user_input_2 == 'Yes'\
                    or user_input_2 == 'yes':
                print(my_game())
            elif user_input_2 == 'NO' or user_input_2 == 'N' or user_input_2 == 'n' or user_input_2 == 'No'\
                    or user_input_2 == 'no':
                print('Ok, bye-bye')
                break
            else:
                print("""Oh, man... Let me help you: 
                        If you want to start game again, use: 'YES' or 'Y' or 'y' or 'Yes', or 'yes'.
                        If you don't want to play again, input: 'NO' or 'N' or 'n' or 'No', or 'no'""")
    elif random_phrase == 3:
            print('Booooring, try your best.')
            print(my_game())
    else:
        print('Ha-ha, you can try again:')
        print(my_game())
print(my_game())

#Home Work 5\ 3 part
def my_function_dict(user_input = input('Please, input something: ')):
    #print(user_input)
    num_spaces = user_input.count(' ')
    num_spaces = {0: num_spaces}
    all_punct = '''!()-[]{};?@#$%:'"\,./^&;*_'''
    punct_in_string = set()
    for i in user_input:
        if i in all_punct:
            punct_in_string.add(i)
    punct_in_string = tuple(punct_in_string)
    user_uniq_punct = {'Punctuation':{punct_in_string}}
    user_string = user_input
    for sign in all_punct:
        user_string = user_string.replace(sign, '')
    user_string = user_string.lower().strip(' ').split()
    user_list = []
    for wlen in user_string:
        user_list.append(len(wlen))
    user_list = set(user_list)
    let_numb = {}
    final_user_list = tuple(user_list)
    for wrd in user_string:
        for lnwrd in final_user_list:
            if len(wrd) == lnwrd:
                if lnwrd in let_numb:
                    let_numb[lnwrd] += [wrd]
                else:
                    let_numb[lnwrd] = [wrd]
    user_dict = {}
    user_dict.update(num_spaces)
    user_dict.update(let_numb)
    user_dict.update(user_uniq_punct)
    return user_dict
print(my_function_dict())