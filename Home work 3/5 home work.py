#H/W 6 //1
def get_year():
    while True:
        try:
            user_age = int(input('Добрый день, введите, пожалуйста, Ваш возраст:'))
            if user_age > 0:
                break
            else:
                print("Введите реальный возраст.")
        except ValueError:
            print("Не понимаю.")
    if user_age in range(10, 20):
        year_string = 'лет'
    elif user_age == 1 or user_age % 10 == 1:
        year_string = 'год'
    elif user_age % 10 == 2 or user_age % 10 == 3 or user_age % 10 == 4:
        year_string = 'года'
    else:
        year_string = 'лет'
    return(year_string, user_age)

def get_answer():
    year_data = get_year()
    if year_data[1] >= 7 and year_data[1] < 18:
        answer = (f'Тебе {year_data[1]} {year_data[0]}, а мы не продаем сигареты несовершеннолетним!')
    elif year_data[1] > 65:
        answer = (f'Вам уже {year_data[1]} {year_data[0]}, Вы в зоне риска!')
    elif year_data[1] < 7:
        answer = (f'Тебе {year_data[1]} {year_data[0]}, где твои мама и папа?')
    else:
        answer = (f'Оденьте маску, вам же {year_data[1]} {year_data[0]}')
    return(answer)
    
result = get_answer()
print(result)
