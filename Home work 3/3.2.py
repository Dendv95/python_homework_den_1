#3.2
while True:
    user_str = input('Введите строку: ')
    user_str = user_str.lower()
    user_list = user_str.split(' ')
    new_list = []
    for i in user_list:
        if i.endswith('о') or i.endswith('o'):
            new_list.append(i)
    letter_counter = len(new_list)
    if letter_counter > 0:
        print(f'Количество слов, что заканчиваются на букву "о" в вашей строке: {letter_counter} ')
        break
    else:
        print('В вашей строке отсутствуют слова, что заканчиваются на букву "о".')
        print('(Подсказка: Программа использует только английский, русский и украинский языки.')