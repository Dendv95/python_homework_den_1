while True:
    user_str = input("Please, input your string and we'll find the smallest word with 2 vowels in a row. Use only English: ")
    print(user_str)
    user_str = user_str.lower().strip(' ')
    import re
    try:
        user_lst = re.findall(r'\w*[aeiouy]{2}\w*', user_str)
        new_user_list = []
        user_min_len = min(user_lst, key=len)
        new_user_list.append(user_min_len)
        user_lst.remove(user_min_len)
        break
    except:
        print("Seems your string don't include words with 2 vowels in a row. Use only English and try again.")
try:
    user_min_len = min(user_lst, key=len)
    if True:
        while len(new_user_list[0]) == len(user_min_len):
            user_min_len = min(user_lst, key=len)
            new_user_list.append(user_min_len)
            user_lst.remove(user_min_len)
    if len(new_user_list[0]) < len(new_user_list[-1]):
        new_user_list.remove(new_user_list[-1])
except:
    new_user_min_len = len(min(new_user_list, key=len))
    try:
        if (len(new_user_list[1])) == (len(new_user_list[0])):
            print(f'Dear, in your string a few words with the same length and with 2 vowels in a row: {new_user_list}.' \
            f'The length of each of these words is: {new_user_min_len}')
    except:
        print(f'Dear, the smallest word with 2 vowels in a row in your string is: {new_user_list}.'\
                f'The length of that word is: {new_user_min_len}')