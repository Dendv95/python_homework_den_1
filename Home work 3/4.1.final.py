#PART 1 ()
import re
user_str =  'User, moon, bi, gg, great, key, may, participate, organization, clean, mean, way, geen agree, missunderstanding.'
print(user_str)
user_str = user_str.lower().strip(' ')
user_lst = re.findall(r'\w*[aeiouy]{2}\w*', user_str)
new_user_list = []
user_min_len = min(user_lst, key=len)
new_user_list.append(user_min_len)
user_lst.remove(user_min_len)
user_min_len = min(user_lst, key=len)
while len(new_user_list[0]) == len(user_min_len):
    user_min_len = min(user_lst, key=len)
    new_user_list.append(user_min_len)
    user_lst.remove(user_min_len)
if len(new_user_list[0]) < len(new_user_list[-1]):
    new_user_list.remove(new_user_list[-1])
new_user_min_len = len(min(new_user_list, key=len))
try:
    if (len(new_user_list[1])) == (len(new_user_list[0])):
        print(f'Dear, in your string a few words with the same length and with 2 vowels in a row: {new_user_list}.' \
           f'The length of each of these words is: {new_user_min_len}')
except:
    print(f'Dear, the smallest word with 2 vowels in a row in your string is: {new_user_list}'\
            f'The length of that word is: {new_user_min_len}')

# PART 2
lower_limit = 35.9
upper_limit = 37.3
user_dict = {
    "citrus": 47.999,
    "istudio": 42.999,
    "moyo": 49.999,
    "royal-service": 37.245,
    "buy.ua": 38.324,
    "g-store": 37.166,
    "ipartner": 38.988,
    "sota": 37.720,
    "rozetka": 38.003,
}
print(user_dict)
a = (list((key for key, values in user_dict.items() if values >= lower_limit and values <= upper_limit)))
print(f'Match: {a}')