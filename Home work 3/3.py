# 3.1
while True:
    user_word = input('Пожалуйста, введите слово минимум из 5 букв:')
    user_word = user_word.strip('  ')
    if user_word.isalpha() and len(user_word) >= 5:
        break
    else:
        print('Ошибка ввода. Слово должно содержать только буквы и состоять не менее чем из 5 символов.')
len_user_word = len(user_word)
while True:
    try:
        symbol_number = int(input('Что бы узнать значение символа в слове, введите его порядковый номер.' \
                                  'Отсчет символов начинается с "0": '))
        letter = (user_word[symbol_number])
        if symbol_number >= 0 and symbol_number <= len_user_word:
            print(f'Символ {symbol_number} в слове "{user_word}" это буква {letter}')
            break
        elif symbol_number < 0:
            print('Номер символа не может быть отрицательным')
    except:
        print(f'Ошибка ввода. Введите целое число, что не превышает количество символов в слове {user_word}')
        print(f'(Подсказка, в этом слове всего {len_user_word} символов)')

