# Home Work 13
import requests
import datetime

class Exchange_Requestor:

  _foreign_currency = 'USD'
  _date = '20211220'

  def __init__(self, for_cur = None, new_date = None):
    if for_cur is not None:
      self.foreign_currency = for_cur
    if new_date is not None:
      self.date = new_date

  @property
  def foreign_currency(self):
    return self._foreign_currency

  @foreign_currency.setter
  def foreign_currency(self, value):
    """
    Проверяет корректный ввод данных по валюте.

    Returns: устанаваливает наименование валюты введенное пользователем объекту класса.

    """
    if not isinstance(value, (str)):
      raise TypeError
    elif len(value) != 3:
      raise 'Please, use Currency abbreviation (three-letter code). For example "USD"'
    value = value.upper()
    self._foreign_currency = value

  @property
  def date(self):
    return self._date

  @date.setter
  def date(self, value):
    """
    Проверяет корректный ввод данных по дате.

    Returns: устанаваливает дату введенную пользователем объекту класса.

    """
    if not isinstance(value, (str)):
      raise TypeError
    if len(value) != 8:
      raise 'Please, input date without dividers YYYYMMDD'
    self._date = value


  def check_con(self, api):
    '''
      Функция для проверки связи с API. Используется в основных методах.

    '''
    try:
      res = requests.get(self.api)
    except:
      print('Some exception')
    else:
      if 300 > res.status_code >= 200 and res.headers['Content-Type'] == 'application/json; charset=utf-8':
        print('No exception')
        return res
      
  @staticmethod
  def write_file(date, list):
    '''
      Метод для записи актуального на сегодня курса валют к гривне в отдельный файл.

    '''
    with open('exchange_today', 'w') as file:
      file.write(date + '\n')
      file.writelines("%s\n" % m for m in list)
          
          
  def today_cur_rate(self, nat_cur = 'UAH'):
    '''
    Метод для получения актуальной информации по курсу всех валют к гривне на сейчас.

    Returns: в отдельный файл записывает дату и курсы всех валют. Сообщает об этом пользователю.

    '''
    now = datetime.datetime.now()
    date = f'{now.year}{now.month}{now.day}'
    self.api = (f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={date}&json')
    res = self.check_con(self.api)
    json_data = res.json()
    counter = 0
    currency_list = []
    actual_date = json_data[0]['exchangedate']
    for i in json_data:
      counter += 1
      rate = i['rate']
      cur = i['cc']
      answer = f'{counter}. {cur} to {nat_cur}: {rate}'
      currency_list.append(answer)
    self.write_file(actual_date, currency_list)
    return 'You can find today rate of exchange in file "exchange_today".'
      
              
  def user_request(self):
    '''
    Метод использует заданные пользователем объекту валюту и дату и возвращает курс к гривне.
    Returns: курс валюты к гривне на заданную дату. Если такой валюты нет говорит 'Don't know'

    '''
    user_currency = self.foreign_currency
    user_date = self.date
    self.api = (f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={user_date}&json')
    res = self.check_con(self.api)
    json_data = res.json()
    list_of_cur = []
    for i in json_data:
      list_of_cur.append(i['cc'])
      if i['cc'] == user_currency:
        return f"{json_data[0]['exchangedate']} rate of {user_currency} to UAH is: {i['rate']}"
    if user_currency not in list_of_cur:
      return 'Don\'t know'


a = Exchange_Requestor()
print(a.today_cur_rate())
b = Exchange_Requestor('eur', '20211210')
print(b.user_request())
print(b.today_cur_rate())


def user_input():
  """
  Функция спрашивает у пользователя, какую информацию о курсе валют он хочет получить.
  Returns: курс валют на сегодня\на заданную дату

  """
  us_inp = input('Hi, if you want to know actual rate of exchange print "1",'\
          'if you want to got rate of exchange on some exact day print "2": ')
  if us_inp == '1':
    object1 = Exchange_Requestor()
    return object1.today_cur_rate()
  if us_inp == '2':
    date_inp = input('Please, print date without dividers in the following format YYYYMMDD: ')
    curr_inp = input('Please, print currency (f. e. "USD"): ')
    object2 = Exchange_Requestor(curr_inp, date_inp)
    return object2.user_request()

def start():
  '''
  Использует для своей работы функцию user_input, запускает программу, спрашивает у пользователя необходимость
  начать программу снова.
  '''
  while True:
    print(user_input())
    user_inp = input('Want to start again? [Y/N]: ')
    if user_inp in ('YES', 'Y', 'y', 'Yes', 'yes'):
      print(user_input())
    else:
      break
      return 'Bye-bye'


print(start())
