#Home Work 9
class Engine:
    volume = 2.0
    fuel = 'gas'

    def __init__(self, volume, engine_type):
        self.volume = volume
        self.fuel = engine_type


class Vehicle:
    engine = Engine(400, 'diesel')
    body = 'iron'
    def __init__(self, engine,body):
        self.engine = engine
        self.body = body

class Vessel(Vehicle):
    sound_signal = 'TUDUU'
    speed_knots = 15
    capacity_teu = 4000

    def __init__(self, engine,body, sound_signal, speed_knots, capacity_teu):
        self.engine = engine
        self.body = body
        self.sound_signal = sound_signal
        self.speed_knots = speed_knots
        self.capacity_teu = capacity_teu

    def make_sound(self):
        return f'{self.sound_signal}'
    def overloaded_func(self, teu = 500):
        if self.capacity_teu < teu:
            return 'Sorry, but we can\'t accept your cargo. We already overladen'
        else:
            return 'All is ok, we can load your containers on board'

    def speedometer(self):
        return f'Speed of this vssl is {self.speed_knots} knots.'

class Car(Vehicle):
    sound_signal = 'BIP'
    wheels = 4
    speed_km = 100
    color = 'black'

    def __init__(self, engine,body, sound_signal, wheels, speed_km, color):
        self.engine = engine
        self.body = body
        self.sound_signal = sound_signal
        self.speed_km = speed_km
        self.wheels = wheels
        self.color = color

    def speed_limit(self, permitted_speed = 60):
        if self.speed_km > permitted_speed:
            return f'Your speed is higher than permitted on {self.speed_km - permitted_speed} km\h.'\
             'Please, slow down'
        else:
            return f'{self.speed_km} km\h is ok, you can keep moving.'

    def make_beep_beep(self):
        return f'{self.sound_signal}, {self.sound_signal}'

class Plane(Vehicle):
    tas_mile = 300
    chassis_system = 'three-post'
    wings = 2

    def __init__(self, engine, body, tas_mile, chassis_system, wings):
            self.engine = engine
            self.body = body
            self.TAS_mile = tas_mile
            self.chassis_system = chassis_system
            self.wings = wings

    def panic_func(self):
        if self.wings < 2:
            return f'Man, what\'s wrong with this plane. Only {self.wings} wings?'\
                    'I will by ticket on the next flight'
        elif self.TAS_mile < 50:
            return 'AHHAAAHAHAHA'
        else:
           return 'What a great flight!'

engine_car = Engine(2.4, 'gas')
car1 = Car(engine_car, 'aluminum', 'BEEP', 4, 70, 'white')
print(car1.speed_km)
print(car1.engine.volume)
print(car1.make_beep_beep())
print(car1.speed_limit())

engine_vessel = Engine(500, 'diesel')
vssl1 = Vessel(engine_vessel, 'iron', 'TUDUU', 15, 1000)
print(vssl1.engine.fuel)
print(vssl1.body)
print(vssl1.overloaded_func())
print(vssl1.speedometer())

engine_plane = Engine(7300, 'aviation kerosene')
plane1 = Plane(engine_plane, 'iron', 300, 'three-post', 2)
print(plane1.engine.fuel)
print(plane1.tas_mile)
print(plane1.chassis_system)
print(plane1.panic_func())

