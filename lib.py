# Функция для ДЗ №6
def y_n_funct():
    """
    Данная функция задает пользователю вопрос, хочет ли
    он повторить ввод строки.
    Returns:
        При ответе Y - отдает True
        При ответе N - отдает False
    """
    while True:
        user_input = input('Do you want to repeat input? [Y/N]: ')
        if user_input == 'YES' or user_input == 'Y' or user_input == 'y' or user_input == 'Yes'\
                or user_input == 'yes':
            return True
        elif user_input == 'NO' or user_input == 'N' or user_input == 'n' or user_input == 'No'\
                or user_input == 'no':
            return False
        else:
            print('Don\'t understand you. Please, try again.')
