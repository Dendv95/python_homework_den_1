# Home Work #7
# Переписал свою прошлую игру, добавил всн требования из 7го дз.
from random import randint
def greetings_funct(greetings, rules):
    """
    Данная функция приветствует Пользователя и объясняет правила игры.
    Returns:
        Возвращает текст
    """
    print("\033[31m {}\033[0m" .format(greetings))
    print("\033[1m {}\033[0m" .format(rules))
greetings_funct(('Hi!! Nice to meet you in my game.'), ('You have 5 attempts to guess the number that i thought!'\
                'Funny, right?'))
def y_n_funct():
    """
    Данная функция задает пользователю вопрос, хочет ли
    он повторить ввод строки.
    Returns:
        При ответе Y - отдает True
        При ответе N - отдает False
    """
    counter = 0
    while True:
        counter += 1
        user_input = input('Want to try again? [Y/N]: ')
        if user_input in ('YES', 'Y', 'y', 'Yes', 'yes'):
            return True
        elif user_input in ('NO', 'N', 'n', 'No', 'no'):
            return False
        elif counter == 3:
            print("""Oh, man... Let me help you: 
                        If you want to start game again, use: 'YES' or 'Y' or 'y' or 'Yes', or 'yes'.
                        If you don't want to play again, input: 'NO' or 'N' or 'n' or 'No', or 'no'""")

        else:
            print('Don\'t understand you. Please, try again.')

def func_user_number():
    """
    Данная функция просит пользователя ввести число от 1 до 100.
    Так же выполняет проверку на корректность введенных данных
    Returns:
        user_number = число пользователя
    """
    while True:
        user_number = input('Choose your number from 1 to 100: ')
        try:
            user_number = int(user_number)
            if user_number > 100 or user_number <= 0:
                print(f'Your number is out of range!')
            else:
                break
        except ValueError:
            print(f'{user_number} is not a number. Please, choose number from 1 to 100.')
    return user_number
    
def attempts_funct(user_number, random_number, user_attempts):
    """
    Функция сравнивает введенное пользователем число со случайным числом.
    Ведет учет попыток пользователя угадать случайное число.
    Returns:
        Возвращает реакцию на введенное число при заданных условиях и 
        оставшееся количество попыток
    """
    conditions_1 = (
    user_number - 5 >= random_number >= user_number - 10,
    user_number + 5 <= random_number <= user_number + 10,
    )
    conditions_2 = (
    user_number - 1 >= random_number >= user_number - 4,
    user_number + 1 <= random_number <= user_number + 4 > random_number
    )
    if any(conditions_2):
        print(f'HOT HOT HOT. Only {user_attempts} attempts left.')
    elif any(conditions_1):
        print(f'Warm! But you have only {user_attempts} attempts left')
    else:
        print(f'COLD and boooooring!!! Try again, you have {user_attempts} attempts')

def my_game():
    """
    Функция создает случайное число и запускает игру с пользователем.
    Для работы использует функции y_n_funct, func_user_number и attempts_funct.
    Returns:
        Если позльозватель хочет повторить игру перезапускает саму себя, если нет -
        останавливает работу программы.
    """
    print('I\'ve already choose my number. If you will match it - you won!')
    random_number = randint(1, 100)
    print(random_number)
    user_number = func_user_number()
    counter = 0
    user_attempts = 5
    if random_number == user_number:
        print('Holy shit!! From the first try!!')
    while random_number != user_number:
        counter += 1
        user_attempts -= 1
        if user_attempts == 0:
            print('HA-HA, Loooser!!')
            break
        attempts_funct(user_number, random_number, user_attempts)
        user_number = func_user_number()
    if random_number == user_number:
        print('Great!! You did it!!')
    if y_n_funct() == False:
        print('Ok. I\'m sad(')
    else:
        print('Ok, let\'s go!!')
        my_game()
    return 'Bye-bye'
print(my_game())
