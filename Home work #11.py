# Home Work 11
import time

def only_numb(func):
    """
    Данная функция используется как декоратор для проверки ввода только цифр (int\float)
    Args:
        func: функция отданная под декоратор

    Returns: в случае успешной проверки переходит к выполнению функции, в противном случае выдает TypeError

    """
    def _wrapper(*args, **kwargs):
        for arg in args:
            if not isinstance(arg, (int, float)):
                raise TypeError
        for arg in kwargs.values():
            if not isinstance(arg, (int, float)):
                raise TypeError

        res = func(*args, **kwargs)
        return res

    return _wrapper



def check_time(time_type):
    """
    Данная функция используется как декоратор и засекает полное время выполнения работы функции
    Примечание: не процессорное время, учитывает время паузы.

    Args:
        time_type: в аргумент следует передавать строки:
        'ms' - для вывода времени в милесекундах
        's' - для вывода времени в секундах
        'm' - для вывода времени в минутах
        'h' - для вывода времени в часах

    Returns: время работы функции

    """
    def _wrapper_outer(func):

        def _wrapper_inner(*f_args, **f_kwargs):
            start = time.monotonic()
            res = func(*f_args, **f_kwargs)
            end = time.monotonic() - start
            if time_type == 'm':
                end = end / 60
                print(end)
            elif time_type == 'h':
                end = end / 3600
                print(end)
            elif time_type == 's':
                print(end)
            elif time_type == 'ms':
                end = end * 1000
                print(end)
            else:
                raise TypeError
            return res

        return _wrapper_inner

    return _wrapper_outer


@only_numb
@check_time('s')
def func(*f_args, **f_kwargs):
    """
    Функция создана для проверки работы декораторов на засекание времени работы и для проверки ввода аргументов
    числового типа.
    В функции специально использована сложность O(n**k) для более длительного вычисления.
    Args:
        *f_args: неименованные аргументы int\float
        **f_kwargs: именованные аргументы int\float

    Returns: демонстрация работы декораторов check_time и only_numb

    """
    pause = 3
    time.sleep(pause)
    my_list = []
    for i in f_args:
        a = (i + 1) ** 4 ** 4
        my_list.append(a)
    for j in f_kwargs.values():
        b = (j + 1)**2 ** 2 ** 2 ** 2
        my_list.append(b)
    print(my_list)
    counter = 0
    new_list = []
    while True:
        counter += 1
        if counter > 5:
            break
        print('__next__')
        for k in my_list:
            k = k ** 2
            new_list.append(k)
            print(new_list)
        print(' after __next__')


func(3, 4, 2, b = 5)

