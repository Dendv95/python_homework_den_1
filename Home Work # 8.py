# Home Work 8
# От себя попробовал разобраться, интерпретировать и использовать интересные моменты с прошлой лекции.
class Animals:
    heterotrophicity = True
    active_movement = True
    alive = True
    voice = True
    name = True

class Birds(Animals):
    wings = 2
    paws = 2
    can_fly = True

    def __init__(self):
        self.wings = 2
        self.paws = 2
        self.voice = 'chirik'
        self.name = 'Orel'
        self.can_fly = True
    def make_voice(self):
        return f'{self.voice}'

class Fish(Animals):
    cover = 'scales'
    can_swim = True
    breathe_uder_water = True

    def __init__(self):
        self.name = 'Zolotaja rybka'
        self.can_swim = True
        self.breathe_uder_water = True
        self.cover = 'scales'
        self.voice = None

class Mammals(Animals):
    paws = 4
    voice = 'meuh'
    def __init__(self, new_paws=None, new_voice=None):
        """
         Функция дает возможность инициализировать количество лап и издаваемый животным звук
        Args:
            new_paws: количество лап, ограничение по четному количеству, аргумент должен быть int
            new_voice: издаваемый звук, ограничение - аругмент должен быть строкой.
        """
        if new_paws is not None:
            if not isinstance(new_paws, int):
                print('new_paws should be an integer')
            elif new_paws % 2 != 0:
                print(f'Nature likes symmetry, dude. I don\'t know which animal has {new_paws} paws.')
            else:
                self.paws = new_paws
        if new_voice is not None:
            if not isinstance(new_voice, str):
                print('new_voice should be a string')
            else:
                self.voice = new_voice

    def make_voice(self):
        return f'{self.voice}'

dog = Mammals(4, "Gav")
dog.name = 'Bobik'
dog_dict = vars(dog)
print(dog_dict)
print(dog.make_voice())

cat = Mammals()
print(cat.make_voice())
cat_dict = vars(cat)
print(cat_dict)

bird = Birds()
bird.name = 'Papuga'
bird_dict = vars(bird)
print(bird_dict)
print(bird.make_voice())
zol_ryb_dict = vars(Fish())
print(zol_ryb_dict)


print('Hello, kid!! Do you want to visit our ZOO? I can show you some animals.'
      'I will try to guess.')

def func_animal_def(can_fly, breathe_under_water, operator = ('bird', 'fish', 'mammal')):
    """
    Функция определяет, является ли животное птицей, рыбой или млекопетающим.
    Создана для работы с функцией user_animal
    Args:
        can_fly: bool
        breathe_under_water: bool
        operator: задан по подвидам животных

    Returns: str, наименование вида животного

    """
    zoo_map = {
        'bird': {
            'validator': lambda x, y: x * 2 + y == 2},
        'fish': {
            'validator': lambda x, y: x + y == 1},
        'mammal': {
            'validator': lambda x, y: x + y == 0}
    }
    for i in zoo_map:
        if i in operator:
            dic = zoo_map[i]
            if dic['validator'](can_fly, breathe_under_water):
                return i

def user_animal():
    """
    Функция задает пользователю вопросы, какое животное он хотел бы увидеть
    Работает с функцией func_animal_def
    Returns: str, наименование вида животного

    """
    can_fly = input('Does it fly? [Y/N]: ')
    if can_fly != 'Y':
        can_fly = False
    else:
        can_fly = True
    breathe_under_water = input('Does it breathe under the water? [Y/N]: ')
    if breathe_under_water != 'Y':
        breathe_under_water = False
    else:
        breathe_under_water = True
    return func_animal_def(can_fly, breathe_under_water)

def zoo_answer():
    """
    Функция использует user_animal и func_animal_def

    Returns: Возвращает подходящий ответ системы под заданные пользователем условия

    """
    animal_type = user_animal()
    if animal_type == 'fish':
        return f'Great, here is aquarium with golden fish, read it:{zol_ryb_dict}'
    elif animal_type == 'bird':
        return f'Great, here is our birds, do you hear them? :{bird.make_voice()}'
    elif animal_type == 'mammal':
        return f'Look! Here is our dog {dog.name}!!'
    else:
        return "Grhhmmm, we don\'t have such animal!"
print(zoo_answer())