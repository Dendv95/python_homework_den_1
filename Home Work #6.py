#Home Work 6 \ 1
def get_year():
    """
    Функция запрашивает возраст пользователя и возвращает
    подходящий под его возраст падеж.
    Returns:
        year_string - падеж
        user_age - возраст
    """
    while True:
        try:
            user_age = int(input('Добрый день, введите, пожалуйста, Ваш возраст:'))
            if user_age > 0:
                break
            else:
                print("Введите реальный возраст.")
        except ValueError:
            print("Не понимаю.")
    if user_age in range(10, 20):
        year_string = 'лет'
    elif user_age == 1 or user_age % 10 == 1:
        year_string = 'год'
    elif user_age % 10 == 2 or user_age % 10 == 3 or user_age % 10 == 4:
        year_string = 'года'
    else:
        year_string = 'лет'
    return year_string, user_age

def get_answer():
    """
    Функция выдает заготовленый ответ согласно возраста пользователи
    Используется вместе с функцией get_year, что позволяет корректно
    обратиться к пользователю по падежу.
    Returns:
        answer - ответ
    """
    year_data = get_year()
    if year_data[1] >= 7 and year_data[1] < 18:
        answer = f'Тебе {year_data[1]} {year_data[0]}, а мы не продаем сигареты несовершеннолетним!'
    elif year_data[1] > 65:
        answer = f'Вам уже {year_data[1]} {year_data[0]}, Вы в зоне риска!'
    elif year_data[1] < 7:
        answer = f'Тебе {year_data[1]} {year_data[0]}, где твои мама и папа?'
    else:
        answer = f'Оденьте маску, вам же {year_data[1]} {year_data[0]}'
    return answer

result = get_answer()
print(result)

#Home Work 6 \ 2
from lib import y_n_funct

def user_string():
    """
    Используется с функцией y_n_funct, что лежит в файле lib.py.
    При помощи данной функции, пользователь вводит строку,
    после чего в случае если ему необходимо повторить ввод
    вводит Y, либо же N и действие заканчивается.
    Returns:
        После окончания работы прощается с пользователем.
    """
    while True:
        input('Please, input your string: ')
        if y_n_funct() is False:
            break
    return "Bye - bye"

result_2 = user_string()
print(result_2)
