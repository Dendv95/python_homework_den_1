# Home Work 2
user_name = input('Добрый день, пожалуйста, введите Ваше имя:')
user_data = input(f'Уважаемый {user_name}, пожалуйста, введите Ваш возраст:')
print(user_data)
user_age = int(user_data)
while user_age <= 0:
    print('Данные введены неверно, введите Ваш реальный возраст')
    user_data = input(f'Уважаемый {user_name}, пожалуйста, введите Ваш возраст:')
    print(user_data)
    user_age = int(user_data)
    if user_age > 0:
        break
if user_age % 10 == 0:
    print(f'{user_name}, где сертификат о вакцинации?!')
elif user_age >= 7 and user_age < 18:
    print(f'{user_name}, мы не продаем сигареты несовершеннолетним!')
elif user_age > 65:
    print(f'{user_name}, Вы в зоне риска.')
elif user_age < 7:
    print(f'{user_name}, где твои мама и папа?')
else:
    print(f'{user_name}, оденьте маску!')