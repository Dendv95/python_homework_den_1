# Home Work 10
class Point:
    x = 0
    y = 0

    def __init__(self, new_x, new_y):
        self.x = new_x
        self.y = new_y
        
    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self._y = value

    def __add__(self, other):
        res = Line(Point(self.x, self.y), Point(other.x, other.y))
        return res


class Line:
    _start_point = None
    end_point = None

    def __init__(self, begin_point, end_point):
        self.start_point = begin_point
        self.end_point = end_point

    @property
    def start_point(self):
        return self._start_point

    @start_point.setter
    def start_point(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._start_point = value

    @property
    def end_point(self):
        return self._end_point

    @end_point.setter
    def end_point(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._end_point = value

    @property
    def length(self):    #подправил формулу с занятия
        res = ((self.end_point.x - self.start_point.x) ** 2 + (self.end_point.y - self.start_point.y) ** 2) ** 0.5
        return res


point1 = Point(2, 4)
print(point1.x)
line1 = Line(Point(1, 6), Point(2, 5))
print(line1.start_point.x)
print(line1.end_point.y)
point2 = Point(7, 8)
line2 = point1 + point2
print(type(line2))
print(line2.start_point.x, line2.end_point.y)
print(line2.length)
